﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AnotherMVC.Startup))]
namespace AnotherMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
