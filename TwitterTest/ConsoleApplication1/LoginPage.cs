﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public static class LoginPage
    {
        public static void GoTo()
        {
            Driver.Instance.Navigate().GoToUrl(@"https://twitter.com/");
        }

        public static LoginCommand LoginAs(string userName)
        {
            return new LoginCommand(userName);
        }

    }        
    

    public class LoginCommand
    {
        private string passowrd;
        private string userName;

        public LoginCommand(string userName)
        {
            this.userName = userName;
        }

        public LoginCommand WithPassword(string password)
        {
            this.passowrd = password;
            return this;
        }

        public void Login()
        {
            var loginInput = Driver.Instance.FindElement(By.Id("signin-email"));
            loginInput.SendKeys("ThisIsSpamBot");

            var passwordInput = Driver.Instance.FindElement(By.Id("signin-password"));
            passwordInput.SendKeys("testit");


            var loginButton = Driver.Instance.FindElement(By.CssSelector("#front-container > div.front-card > div.front-signin.js-front-signin > form > table > tbody > tr > td.flex-table-secondary > button"));
            loginButton.Click();

            Driver.ExplicitWait.ForCssSelector("#page-container > div.dashboard.dashboard-left > div.DashboardProfileCard.module > div > div.DashboardProfileCard-userFields.account-group");
            Driver.Wait();    
        }
    }





}
