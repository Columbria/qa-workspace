﻿using OpenQA.Selenium;
using System.Linq;
using System.Threading;
using System;

namespace TwitterAutomation
{
    public static class DashboardPage
    {
        public static bool IsAt
        {
            get
            {
                var tags = Driver.Instance.FindElements(By.TagName("a"))
                    .Where(o => o.Text == "Test Mania");
       
                return (tags != null) ? true : false;
            }
        }


        public static bool TweetIsPosted(string text)
        {
                var tags = Driver.Instance.FindElements(By.TagName("p"))
                    .Where(o => o.Text == text);

                return (tags != null) ? true : false;
        }

        public static void Tweet(string text)
        {
           // Driver.ExplicitWait.ForId("#tweet-box-home-timeline > div");
            var writeTweetButton = Driver.Instance.FindElement(By.Id("global-new-tweet-button"));

            writeTweetButton.Click();
            var tweetBox = Driver.Instance.FindElement(By.Id("tweet-box-global"));
            tweetBox.Click();
            tweetBox.Clear();
            tweetBox.SendKeys(text);


            var tweetButton = Driver.Instance.FindElement(By.CssSelector(".modal-tweet-form-container > form:nth-child(1) > div:nth-child(3) > div:nth-child(2) > button:nth-child(3)"));
            tweetButton.Click();

            Driver.ExplicitWait.ForCssSelector("#page-container > div.dashboard.dashboard-left > div.DashboardProfileCard.module > div > div.DashboardProfileCard-userFields.account-group");
            Driver.Wait();


        }
    }
}