﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TwitterAutomation
{
    public static class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            //Goes to .exe file and then 3 folders up for FireFox driver
            var path = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\");
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(path);
            Instance = new FirefoxDriver(service);
        }

        public static void Close()
        {
            Instance.Quit();
        }

        public static void Wait()
        {
            Thread.Sleep(100);
        }

        //#Region for Click() experiments

        public static class ExplicitWait
        {
            public static void ForId(string idString)
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
                IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.Id(idString)));
            }

            public static void ForClass(string classString)
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
                IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.ClassName(classString)));
            }

            public static void ForName(string className)
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
                IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.Name(className)));
            }

            public static void ForCssSelector(string cssSelector)
            {
                WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
                IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.CssSelector(cssSelector)));
            }


        }




    }




}

