﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwitterAutomation;
using System.Threading;

namespace TwitterTest
{
    [TestClass]
    public class LoginTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void Sys_User_Can_Login()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("ThisIsSpamBot")
                .WithPassword("testit")
                .Login();

            Assert.IsTrue(DashboardPage.IsAt, "Failed to login");
        }



        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }

    }
}
