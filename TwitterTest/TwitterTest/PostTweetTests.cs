﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TwitterAutomation;

namespace TwitterTest
{
    [TestClass]
    public class PostTweetTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }

        [TestMethod]
        public void User_Can_Tweet()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("ThisIsSpamBot")
                .WithPassword("testit")
                .Login();

            string text = "This is a test " + DateTime.Now.ToString("MM/dd/yyyy h:mm:ss tt");
            DashboardPage.Tweet(text);

            Assert.IsTrue(DashboardPage.TweetIsPosted("asdfasdfa"), "Failed to tweet");

        }



        [TestCleanup]
        public void 
            
            Cleanup()
        {
            Driver.Close();
        }

    }
}
