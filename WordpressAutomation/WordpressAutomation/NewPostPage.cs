﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WordpressAutomation
{
    public class NewPostsPage
    {
        public static void GoTo()
        {
            var menuPosts = Driver.Instance.FindElement(By.Id("menu-posts"));
            menuPosts.Click();

            ExplicitWait.ForClass("page-title-action");

            var addNew = Driver.Instance.FindElement(By.ClassName("page-title-action"));
            addNew.Click();
        }

        public static CreatePostCommand CreatePost(string title)
        {
            return new CreatePostCommand(title);
        }

        public static void GoToNewPost()
        {

            ExplicitWait.ForId("message");
            var message = Driver.Instance.FindElement(By.Id("message"));
            var newPostlink = message.FindElements(By.TagName("a"))[0];
            newPostlink.Click();
        }

        public static bool IsInEditMode()
        {          
            return Driver.Instance.FindElement(By.Id("icon-edit-pages")) != null;
        }

        public static string Title
        {
            get
            {
                var title = Driver.Instance.FindElement(By.Id("title"));
                if (Title != null)
                    return title.GetAttribute("value");
                return string.Empty;
            }
        }
    }

        public class CreatePostCommand
        {
            private readonly string title;
            private string body;
            public CreatePostCommand(string title)
            {
                this.title = title;
            }

            public CreatePostCommand WithBody(string body)
            {
                this.body = body;
                return this;
            }

            public void Publish()
            {
            Thread.Sleep(2000);

            Driver.Instance.FindElement(By.Id("title")).SendKeys(title);

                Driver.Instance.SwitchTo().Frame("content_ifr");
                Driver.Instance.SwitchTo().ActiveElement().SendKeys(body);
                Driver.Instance.SwitchTo().DefaultContent();

            Thread.Sleep(2000);

            Driver.Instance.FindElement(By.Id("publish")).Click();

             
            }
        }

        
}

