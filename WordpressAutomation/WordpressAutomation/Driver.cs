﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace WordpressAutomation
{
    public class Driver
    {
        public static IWebDriver Instance { get; set; }

        public static void Initialize()
        {
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService(@"C:\Libs\BrowserDrivers");
            Instance = new FirefoxDriver(service);
        }


        
        public static void Close()
        {
            Instance.Quit();
        }
    }


    public static class ExplicitWait
    {
        public static void ForId(string idString)
        {
            WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.Id(idString)));
        }

        public static void ForClass(string classString)
        {
            WebDriverWait wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(10));
            IWebElement myDynamicElement = wait.Until(d => d.FindElement(By.ClassName(classString)));
        }

    }

}