﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordpressAutomation;
using System.Threading;

namespace WordpressTests
{
    [TestClass]
    public class LoginTests
    {
        [TestInitialize]
        public void Init()
        {
            Driver.Initialize();
        }


        [TestMethod]
        public void Sys_User_Can_Login()
        {
            LoginPage.GoTo();
            LoginPage.LoginAs("sys")
                .WithPassword("sys")
                .Login();

            // Driver.ExplicitWaitForId("adminmenu");
            ExplicitWait.ForId("adminmenu");

            Assert.IsTrue(DashboadPage.IsAt, "Failed to login");
        }




        [TestCleanup]
        public void Cleanup()
        {
            Driver.Close();
        }
  

        
    }
}
